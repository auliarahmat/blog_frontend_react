import TopBar from "./components/topbar/topbar";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";

import Home from '../src/pages/home/home'
import Register from '../src/pages/register/Register'
import Login from '../src/pages/login/Login';
import Write from '../src/pages/write/Write'
import Settings from '../src/pages/settings/Settings'
import SinglePost from '../src/pages/single/Single'
import { useContext } from "react";
import { Context } from "./context/Context";

function App() {
    const {user} = useContext(Context)
    return (
        <Router>
            <TopBar />
            <Switch>
                <Route exact path='/'> <Home /> </Route>

                <Route path='/register'> <Register /> </Route>

                <Route path='/login'> <Login /> </Route>

                <Route path='/write'> {user ? <Write /> : <Login />} </Route>

                <Route path='/settings'> {user ? <Settings /> : <Login />} </Route>

                <Route path='/post/:postId'> {user ? <SinglePost /> : <Login />} </Route>
            </Switch>
        </Router>
    );
}

export default App;
