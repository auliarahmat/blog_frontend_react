import React, { useState, useEffect } from 'react'

import Header from '../../components/header/header'
import Posts from '../../components/posts/Posts'
import Sidebar from '../../components/sidebar/Sidebar'

import './home.css'

import axios from 'axios'
import { useLocation } from 'react-router'

const Home = () => {
    const [posts, setPosts] = useState([])

    const {search } = useLocation()

    useEffect(() => {
        GetPost()

    }, [search])

    const GetPost = async () => {
        const res = await axios.get('/posts' + search)
        setPosts(res.data);
    }
    return (
        <>
            <Header />
            <div className='home'>
                <Posts posts={posts} />
                <Sidebar />
            </div>
        </>
    )
}

export default Home
