import React from 'react'
import './post.css'
import { Link } from 'react-router-dom'

export default function Post({ data }) {
    const PF = 'http://localhost:5000/images/'

    return (
        <div className='post'>
            {data.photo && (
                <img
                    className='postImg'
                    src={PF + data.photo}
                    alt=''
                />
            )}
            <div className="postInfo">
                <div className="postCats">
                    {
                        data.categories.map((c) => (
                            <span className="postCat">{c.name}</span>
                        ))
                    }
                </div>
                <Link className='link' to={`post/${data._id}`}>
                    <span className="postTitle">
                        {data.title}
                    </span>
                </Link>
                <hr />
                <span className="postDate">{new Date(data.createdAt).toDateString()}</span>
            </div>
            <p className="postDes">
                {data.desc}
            </p>
        </div>
    )
}
