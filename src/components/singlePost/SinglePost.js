import React, { useContext, useEffect, useState } from 'react'
import { useLocation } from 'react-router'
import './singlePost.css'
import axios from 'axios'
import { Link } from 'react-router-dom'
import { Context } from '../../context/Context'

export default function SinglePost() {
    const location = useLocation()
    const path = location.pathname.split('/')[2]
    const PF = 'http://localhost:5000/images/'

    const [singlePost, setSinglePost] = useState([])
    const { user } = useContext(Context)

    const [title, setTitle] = useState('')
    const [desc, setDesc] = useState('')
    const [updateMode, setUpdateMode] = useState(false)

    useEffect(() => {
        getPost()
    }, [path])

    const getPost = async () => {
        const res = await axios.get('/posts/' + path)
        setSinglePost(res.data.data);
        setTitle(res.data.data.title);
        setDesc(res.data.data.desc);
    }

    const handleUpdate = async (e) => {
        try {
            await axios.put('/posts/' + path,
                {
                    username: user.username,
                    title,
                    desc,
                })
            window.location.reload()
        } catch (error) {
            console.log(error);
        }
    }

    const handleDeleted = async (e) => {
        try {
            await axios.delete('/posts/' + path,
                {
                    data: { username: user.username }
                })
            window.location.replace('/')
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <div className='singlePost'>
            <div className="singlePostWrapper">
                {singlePost.photo && (
                    <img
                        src={PF + singlePost.photo}
                        alt=""
                        className="singlePostImg"
                    />
                )}
                {
                    updateMode ?
                        <input
                            type='text'
                            value={title}
                            className="singlePostTitleInput"
                            autoFocus
                            onChange={(e) => setTitle(e.target.value)}
                        />
                        :
                        (
                            <h1 className="singlePostTitle">
                                {singlePost.title}
                                {
                                    singlePost.username === user.username && (
                                        <div className="singlePostEdit">
                                            <i className='singlePostIcon far fa-edit'
                                                onClick={() => setUpdateMode(true)} />
                                            <i className='singlePostIcon far fa-trash-alt'
                                                onClick={handleDeleted} />
                                        </div>

                                    )
                                }
                            </h1>
                        )
                }
                <div className="singlePostInfo">
                    <span className='singlePostAuthor'>Author:
                        <Link className='link' to={`/?user=${singlePost.username}`}>
                            <b> {singlePost.username}</b>
                        </Link>
                    </span>
                    <span className='singlePostDate'>{new Date(singlePost.createdAt).toDateString()}</span>
                </div>
                {
                    updateMode ?
                        (
                            <>
                                <textarea
                                    className='singlePostDescInput'
                                    value={desc}
                                    onChange={(e) => setDesc(e.target.value)}
                                />
                                <button className="singlePostButton" onClick={handleUpdate}>Upload</button>
                            </>
                        )
                        :
                        (
                            <p className='singlePostDesc'>
                                {singlePost.desc}
                            </p>

                        )
                }
            </div>
        </div>
    )
}
