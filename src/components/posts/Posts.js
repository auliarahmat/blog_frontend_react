import React from 'react'
import Post from '../post/Post'
import './posts.css'

const Posts = ({ posts }) => {

    return (
        <div className='posts'>
            {
                posts.data ?
                    posts.data.map((item, idx) => (
                        <Post data={item} />
                    ))
                    :
                    null
            }
        </div>
    )
}

export default Posts
