import axios from 'axios'
import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import './sidebar.css'

export default function Sidebar() {
    const [cats, setCats] = useState([])

    useEffect(() => {
        getCategory()

    }, [])

    const getCategory = async () => {
        const res = await axios.get('/categories')
        setCats(res.data.data)
    }
    return (
        <div className='sidebar'>
            <div className='sidebarItem'>
                <span className='sidebarTitle'>ABOUT ME</span>
                <img src='https://cdn4.iconfinder.com/data/icons/avatars-xmas-giveaway/128/batman_hero_avatar_comics-512.png' alt='' />
                <p>
                    React Native adalah framework yang digunakan untuk membuat mobile app di dua sistem operasi sekaligus,
                    yaitu Android dan iOS.
                </p>
            </div>
            <div className='sidebarItem'>
                <span className='sidebarTitle'>CATEGORIES</span>
                <ul className='sidebarList'>
                    {
                        cats.map((c) => (
                            <Link className='link' to={`/?cat=${c.name}`}>
                                <li className='sidebarListItem'>{c.name}</li>
                            </Link>
                        ))
                    }
                </ul>
            </div>
            <div className='sidebarItem'>
                <span className='sidebarTitle'>FOLLOW US</span>
                <div className='sidebarSocial'>
                    <i className='sidebarIcon fab fa-facebook-square'></i>
                    <i className='sidebarIcon fab fa-twitter-square'></i>
                    <i className='sidebarIcon fab fa-pinterest-square'></i>
                    <i className='sidebarIcon fab fa-instagram-square'></i>
                </div>
            </div>
        </div>
    )
}
